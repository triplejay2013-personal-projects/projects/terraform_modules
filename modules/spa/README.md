# SPA AWS Architecture About

Modules related to setting up a cloud stack in AWS.

This includes templates for:

- hosted zones
- route53 records
- s3 bucket hosts
- acm config
- cloudfront

Note that this setup expects the hosted website to be a SAP model. This means
that all error handling is routed through the same index.

You may use all or some of the provided modules. This is intended to be used
as a single module, and not a wrapper around the individual resources. This
represents all the resources necessary to set up a website (primary or as a
subdomain).

This particular configuration expects that a registered domain exists, and is
manually configured.

# DNSSEC

Unfortunately, there isn't a good way to automate this. In order to use this
feature, create a kms key and add it to your registered hosted zone (if using
AWS registered domains).

I am using a AWS registered domain, which set up a hosted zone which I will manually
maintain. This module allows you to set up resources for an existing hosted zone, or
to create one (and then link to a parent zone) with the same resources.

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
<!-- END_TF_DOCS -->