terraform {
  required_version = ">= 1.0.3"

  required_providers {
    aws = ">= 3.59.0"
  }
}

provider "aws" {
  region = "us-west-2"
}

provider "aws" {
  alias  = "use1"
  region = "us-east-1"
}
