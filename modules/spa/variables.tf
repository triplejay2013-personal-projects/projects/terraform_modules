locals {
  s3_origin_id  = "myS3Origin"
}

variable "acm_certificate_arn" {
  description = "Issued ACM certificate to associate with cloudfront"
  type        = string
}

variable "domains" {
  # type = map
  # I am not sure how to  properly validate this but this expects a map like:
  # domains = {
  #   "domain.com" = {
  #     description = ""
  #     tags = {
  #       Key = "value"
  #     }
  #   }
  # }
}

variable "primary_domain" {
  type = string
}

variable "project_name" {
  description = "The name of project"
  type        = string
}

variable "zone_id" {
  description = "Primary Zone ID (assumes aws-generated domain)"
  type        = string
}
