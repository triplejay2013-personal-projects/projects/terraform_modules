resource "aws_route53_record" "cloudfront_liason_ipv4" {
  for_each = var.domains
  name     = each.key
  type     = "A"
  zone_id  = var.zone_id

  alias {
    name    = aws_cloudfront_distribution.primary_s3_distribution[each.key].domain_name
    zone_id = aws_cloudfront_distribution.primary_s3_distribution[each.key].hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "cloudfront_liason_ipv6" {
  for_each = var.domains
  name     = each.key
  type     = "AAAA"
  zone_id  = var.zone_id

  alias {
    name    = aws_cloudfront_distribution.primary_s3_distribution[each.key].domain_name
    zone_id = aws_cloudfront_distribution.primary_s3_distribution[each.key].hosted_zone_id
    evaluate_target_health = false
  }
}
