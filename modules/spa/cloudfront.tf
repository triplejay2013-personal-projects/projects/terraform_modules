data "aws_cloudfront_cache_policy" "cache" {
  name = "Managed-CachingOptimized"
}

resource "aws_cloudfront_origin_access_identity" "cid" {
  for_each = var.domains
  comment  = "access-identity-${each.key}"
}

resource "aws_cloudfront_distribution" "primary_s3_distribution" {
  for_each            = var.domains
  enabled             = true
  default_root_object = "index.html"
  price_class         = "PriceClass_All"
  aliases             = [each.key]
  is_ipv6_enabled     = true
  http_version        = "http2"

  origin {
    domain_name         = aws_s3_bucket.website_host_bucket[each.key].bucket_regional_domain_name
    origin_id           = aws_s3_bucket.website_host_bucket[each.key].bucket_regional_domain_name
    connection_attempts = 3  # Default 3
    connection_timeout  = 10  # Default 10
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cid[each.key].cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = aws_s3_bucket.website_host_bucket[each.key].bucket_regional_domain_name
    cache_policy_id        = data.aws_cloudfront_cache_policy.cache.id
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  }

  custom_error_response {
    error_code         = 403
    response_page_path = "/index.html"
    response_code      = 200
  }

  custom_error_response {
    error_code         = 404
    response_page_path = "/index.html"
    response_code      = 200
  }

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.log_bucket.bucket_domain_name
    prefix          = var.project_name
  }

  viewer_certificate {
    acm_certificate_arn            = var.acm_certificate_arn
    ssl_support_method             = "sni-only"
    cloudfront_default_certificate = false
    minimum_protocol_version       = "TLSv1.2_2021"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  depends_on = [aws_s3_bucket.log_bucket]

  tags = each.value["tags"]
}
