resource "aws_s3_bucket" "website_host_bucket" {
  for_each = var.domains
  bucket   = each.key

  tags = each.value["tags"]
}

resource "aws_s3_bucket_website_configuration" "website_host_config" {
  for_each = var.domains
  bucket   = aws_s3_bucket.website_host_bucket[each.key].bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_ownership_controls" "website_host_ownership_controls" {
  for_each = var.domains
  bucket   = aws_s3_bucket.website_host_bucket[each.key].id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "website_host_private_bucket_acl" {
  depends_on = [aws_s3_bucket_ownership_controls.website_host_ownership_controls]
  for_each   = var.domains
  bucket     = aws_s3_bucket.website_host_bucket[each.key].bucket
  acl        = "private"
}

resource "aws_s3_bucket_logging" "website_host_bucket_logging" {
  for_each = var.domains
  bucket   = aws_s3_bucket.website_host_bucket[each.key].id

  target_bucket = aws_s3_bucket.log_bucket.id
  target_prefix = "log/cloudfront-s3-logs/"
}

# tfsec:ignore:aws-s3-encryption-customer-key
resource "aws_s3_bucket_server_side_encryption_configuration" "website_host_sse_config_logs" {
  for_each = var.domains
  bucket   = aws_s3_bucket.website_host_bucket[each.key].bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "website_host_bucket_versioning" {
  for_each = var.domains
  bucket   = aws_s3_bucket.website_host_bucket[each.key].bucket

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "website_host_access_block" {
  for_each                = var.domains
  bucket                  = aws_s3_bucket.website_host_bucket[each.key].id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_policy" "website_host_bucket_policy" {
  for_each = var.domains
  bucket   = aws_s3_bucket.website_host_bucket[each.key].id

  policy = jsonencode({
    Version   = "2012-10-17"
    Id        = "CloudFront Access Policy"
    Statement = [
      {
        Sid       = "CloudFront Allow"
        Effect    = "Allow"
        Principal = {"AWS": aws_cloudfront_origin_access_identity.cid[each.key].iam_arn}
        Action    = "s3:GetObject"
        Resource  = ["${aws_s3_bucket.website_host_bucket[each.key].arn}/*"]
      }
    ]
  })
}
