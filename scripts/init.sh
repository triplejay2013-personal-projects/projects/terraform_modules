#!/bin/bash
TF_USEERNAME=${TF_USERNAME:-}
TF_PASSWORD=${TF_PASSWORD:-}
TF_BIN=${TF_BIN:-terraform}
EXTRAS=${EXTRAS:-}

if [[ -z $TF_USERNAME ]] || [[ -z $TF_PASSWORD ]]; then
    echo "Usage: TF_USERNAME=<USER> TF_PASSWORD=<PASS> EXTRAS=<-opts> terraform_init.sh"
    exit 1
fi

$TF_BIN init -backend-config=username=${TF_USERNAME} -backend-config=password=${TF_PASSWORD} ${EXTRAS}
