#!/bin/bash
GIT_ROOT=`git rev-parse --show-toplevel`
for module in modules/*; do
    echo "Checking module '$module'"
    docker run -i --rm -v $GIT_ROOT/$module:/check registry.gitlab.com/triplejay2013-personal-projects/utility/docker/tflint:latest /check --loglevel=info
done
