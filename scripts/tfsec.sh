#!/bin/bash
GIT_ROOT=`git rev-parse --show-toplevel`
docker run -i --rm -v $GIT_ROOT:/check -t aquasec/tfsec check
