#!/bin/bash

GIT_ROOT=`git rev-parse --show-toplevel`
docker run --rm --volume "$GIT_ROOT:/terraform-docs" -w /terraform-docs -u $(id -u) quay.io/terraform-docs/terraform-docs:0.16.0 -c .terraform-docs.yml .
